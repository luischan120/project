from django.db import models
from apps.proveedor.models import Proveedor
from apps.almacen.models import Almacen
# Create your models here.

class Factura(models.Model):
	iddetalle: models.CharField(max_length=30, primary_key=True)
	nombre: models.CharField(max_length=30)
	cantidad: models.IntegerField()
	subtotal: models.FloatField()

class Cuentapago(models.Model):
	codigo = models.CharField(max_length=30, primary_key=True)
	proveedor = models.ForeignKey(Proveedor, null=True, blank=True, on_delete=models.CASCADE)
	total = models.FloatField();
	fecha = models.DateField()
	almacen = models.OneToOneField(Almacen, null=True, blank=True, on_delete=models.CASCADE);
	factura = models.OneToOneField(Factura, null=True, blank=True, on_delete=models.CASCADE)
