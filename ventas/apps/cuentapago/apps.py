from django.apps import AppConfig


class CuentapagoConfig(AppConfig):
    name = 'cuentapago'
