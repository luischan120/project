from django.db import models

class Proveedor(models.Model):
	idproveedor: models.IntegerField(max_length=30, primary_key=True)
	nombre: models.CharField(max_length=30)
	telefono: models.CharField(max_length=30)
	producto: models.CharField(max_length=30)
# Create your models here.
