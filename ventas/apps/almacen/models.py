from django.db import models
from apps.orden.models import Orden
# Create your models here.

class Producto(models.Model):
	iddetalle: models.CharField(max_length=30, primary_key=True)
	nombre: models.CharField(max_length=30)
	cantidad: models.IntegerField()
	subtotal: models.FloatField()

class Almacen(models.Model):
	folio = models.CharField(max_length=30, primary_key=True)
	idalmacen: models.CharField(max_length=30, primary_key=True)
	nombre: models.CharField(max_length=30)
	fecha = models.DateField()
	disponibilidad = models.CharField(max_length=20)
	orden = models.ForeignKey(Orden, null=True, blank=True, on_delete=models.CASCADE)
	producto = models.ManyToManyField(Producto)
