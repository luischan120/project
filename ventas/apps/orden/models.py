from django.db import models
from apps.cliente.models import Cliente

# Create your models here.

class Detorden(models.Model):
	iddetalle: models.CharField(max_length=30, primary_key=True)
	nombre: models.CharField(max_length=30)
	cantidad: models.IntegerField()
	subtotal: models.FloatField()

class Orden(models.Model):
	folio = models.CharField(max_length=30, primary_key=True)
	fecha = models.DateField()
	total = models.FloatField()
	cliente = models.ForeignKey(Cliente, null=True, blank=True, on_delete=models.CASCADE)
	detorden = models.ManyToManyField(Detorden)